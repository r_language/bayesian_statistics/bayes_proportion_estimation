# Bayes_Proportion_Estimation

In this rmd file, you can have a look at an exercise that tries to estimate a proportion thanks to a frequentist approach and to a bayesian approach. Here is the wording : We would like to estimate the diabete II prevalence in the french population. We have a sample of 10 patients selected at random in the french population. Among those 10 patients, 2 have diabete II. We note $\theta$ the proportion of patients who have diabete II in the french population.

Author : Marion Estoup

E-mail : marion_110@hotmail.fr

December 2022
